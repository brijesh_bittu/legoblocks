(function(window) {
  var APP = window.LegoBlocks || {};

  var Droppable = React.createClass({
    getInitialState: function() {
      return {
        className: ''
      };
    },

    getDefaultProps: function() {
      return {
        supportClick: true,
        multiple: false
      };
    },

    getClassName: function() {
      return 'block-droppable '+(this.props.className || this.state.className);
    },

    onDragOver: function(e) {
      e.preventDefault();
      e.stopPropagation();
      e.dataTransfer.dropEffect = 'copy';
      var effectAllowed = e.dataTransfer.effectAllowed;
      if (effectAllowed === 'all' || effectAllowed === 'uninitialized') {
        this.setState({
          className: 'block-drop-active'
        });
      }
      if (this.props.onDragOver) {
        this.props.onDragOver(e);
      }
    },

    onDragLeave: function(e) {
      this.setState({
        className: ''
      });

      if (this.props.onDragLeave) {
        this.props.onDragLeave(e);
      }
    },

    onClick: function() {
      var fileInput = React.findDOMNode(this.refs.input);
      fileInput.value = null;
      fileInput.click();
    },

    onDrop: function(e) {
      e.preventDefault();
      var files;
      if(e.dataTransfer) {
        files = e.dataTransfer.files;
      } else if(e.target) {
        files = e.target.files;
      }
      var maxFiles = (this.props.multiple) ? files.length : 1;
      for (var i = 0; i < maxFiles; i++) {
        files[i].preview = URL.createObjectURL(files[i]);
      }
      if(this.props.onDrop) {
        files = Array.prototype.slice.call(files, 0, maxFiles);
        //console.log(files);
        this.props.onDrop(files);
      }
    },

    render: function() {
      return (
        <div
          className={this.getClassName()}
          onDragOver={this.onDragOver}
          onDragLeave={this.onDragLeave}
          onClick={this.onClick}
          onDrop={this.onDrop} >
          <input
            ref="input"
            type="file"
            style={{display: 'none'}}
            multiple={this.props.multiple}
            onChange={this.onDrop} />
          {this.props.children}
        </div>
      );
    }
  });

  APP.Droppable = Droppable;
  window.LegoBlocks = APP;
})(this);
// function fl(f) {
//   console.log(f);
// }
// var editor = <LegoBlocks.Droppable onDrop={fl}><p>Drop files here or click to upload.</p></LegoBlocks.Droppable>;

// React.render(editor, document.getElementById('editor-content'));
