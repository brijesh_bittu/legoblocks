import os
from flask import Blueprint, request, jsonify, send_from_directory
from werkzeug import secure_filename


UPLOAD_FOLDER = os.path.join(
    os.path.dirname(os.path.dirname(os.path.abspath(__file__))),
    "images"
)
ALLOWED_EXTENSIONS = set(['png', 'jpg', 'jpeg', 'gif'])

img = Blueprint('img', __name__)


def allowed_file(filename):
    return '.' in filename and \
        filename.rsplit('.', 1)[1] in ALLOWED_EXTENSIONS


@img.route("/upload", methods=["POST"])
def upload():
    file = request.files["image"]
    if file and allowed_file(file.filename):
        filename = secure_filename(file.filename)
        file.save(os.path.join(UPLOAD_FOLDER, filename))
        return jsonify(
            type="success", message="http://localhost:5000/img/"+filename)
    else:
        return jsonify(type="error", message="Invalid file.")


@img.route("/<path:path>")
def send_logo(path):
    return send_from_directory(UPLOAD_FOLDER, path)
