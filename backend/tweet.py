import urllib
import json

from flask import Blueprint, request, jsonify

tweet = Blueprint('tweet', __name__)


@tweet.route('/')
def fetch_tweet():
    url = request.args.get('url', '')
    if url == '':
        return jsonify(html='Invaid URL', type='error'), 401
    embed_url = 'https://api.twitter.com/1/statuses/oembed.json?url=' + url
    response = urllib.urlopen(embed_url)
    resp = json.load(response)
    pos = resp['html'].find('<script')
    if pos > 0:
        resp['html'] = resp['html'][:pos-1]
    return jsonify(html=resp['html'], type='success')
